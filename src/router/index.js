import {
  createRouter,
  createWebHistory
} from "vue-router";
const routerHistory = createWebHistory();
const router = createRouter({
  history: routerHistory,
  routes: [{
    path: '/PureMode',
    component: () =>
      import("../pages/PureMode.vue")
  }, {
    path: "/",
    component: () =>
      import("../pages/Index.vue")
  }, {
    path: '/login',
    component: () =>
      import("../pages/UserLogin.vue")
  }, {
    path: '/reg',
    component: () =>
      import("../pages/UserReg.vue")
  }, {
    path: '/:pathMatch(.*)*',
    redirect: '/'
  }]
});

export default router;
import axios from "axios";

const API = axios.create({
  baseURL: "http://49.232.214.76",
});

API.interceptors.request.use((req) => {
  // add token
  if (localStorage.getItem("token")) {
    req.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  }

  return req;
});

/**
 * 查询用户列表
 * @returns promise
 */
export const QueryUser = () => {
  return API.get('/user')
}

/**
 * 用户登录
 * @param {*} data 
 * @returns 
 */
export const Login = (data) => {
  return API.post('/login', data)
}

/**
 * 用户注册
 * @param {*} data 
 * @returns 
 */
export const Reg = (data) => {
  return API.post('/register', data)
}

/**
 * 获取单个用户信息
 * @param {*} username 
 * @returns 
 */
export const QueryUserInfo = (username) => {
  return API.get('/user/findByName/' + username)
}
/**
 * 完成一个todo任务
 * @param {*} tid 
 * @returns 
 */
export const DoneTodo = (tid) => {
  return API.post('/normal/finishTask',{
    tid
  })
}
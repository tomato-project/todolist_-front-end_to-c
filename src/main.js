import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from "./router/index.js"
import './assets/index.css'
import 'animate.css';


createApp(App)
  .use(createPinia())
  .use(router)
  .mount('#app')



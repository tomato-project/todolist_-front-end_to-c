import {
    defineStore
} from "pinia";

export const useTodoListStore = defineStore({
    id: 'todoList',
    state: () => ({
        tdlist: [{
            'tid': '001',
            'title': '学习前端',
            'de': '完成青训营第一课',
            'state': 3
        }, {
            'tid': '002',
            'title': '学习前端',
            'de': '整理青训营第一课笔记',
            'state': 0
        },{
            'tid': '003',
            'title': '学习前端',
            'de': '整理青训营第二课笔记',
            'state': 0
        },{
            'tid': '004',
            'title': '学习前端',
            'de': '整理青训营第三课笔记',
            'state': 0
        },{
            'tid': '005',
            'title': '学习前端',
            'de': '整理青训营第四课笔记',
            'state': 0
        }]
    }),
    actions: {
        changeState(tid, state) {
            for (let i of this.tdlist) {
                if (i.tid == tid) {
                    i.state = state
                }
            }
        }
    }
})
import {
    defineStore
} from "pinia";

export const useTodoStore = defineStore({
    id: 'currentTodo',
    state: () => ({
        tid: '000',
        title: '摸鱼'
    }),
    actions: {
        changeCurrentTodo(tid, title) {
            this.tid = tid
            this.title = title
        }
    }
})
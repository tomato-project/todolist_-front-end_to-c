import { defineStore } from "pinia";

export const useTimeStore = defineStore({
    id: 'Time',
    state: () => {
        return {
            startTime: 2525,
            duration: 0,
            second: 0
        }
    }
})
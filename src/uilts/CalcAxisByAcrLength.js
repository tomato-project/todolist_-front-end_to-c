
/**
 * 
 * @param {int} acrLength 弧长
 * @param {int} r 半径
 * @param {int} cx 圆心x坐标
 * @returns 
 */
export default function (acrLength, r, cx) {
    let rad = calcRad(acrLength, r)
    let cy = +cx
    let y = Math.abs(Math.sin(rad + Math.PI/2)*r - cy)
    // let y = 144
    let x = Math.cos(rad + Math.PI/2)*r +(+cx)
    return {
        x,
        y
    }
}

function calcRad(acrLength, r){
    return acrLength / r
}